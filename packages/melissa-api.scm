(define-module (melissa-api)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages networking)
  #:use-module (guix licenses))

(define-public melissa-api
  (package
   (name "melissa-api")
   (version "1.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.inria.fr/melissa/melissa.git")
                  (commit "090b0b61df0ca2f0ae0453bea75749cd3d5f9198")))
            (sha256
             (base32
;;              "0000000000000000000000000000000000000000000000000000"))))
		"1zv36r8gbb92v9s6vvzgdvv8ryli6a51b0i3g61k1rr6l3bvh6g3"))))
   (build-system cmake-build-system)
   (inputs (list openmpi zeromq gcc-toolchain gfortran-toolchain pkg-config))
   (arguments
    '(
      #:tests? #f))
   (home-page "https://gitlab.inria.fr/melissa/melissa")
   (synopsis "Melissa API for client instrumentation")
   (description
    "Melissa is a file-avoiding, adaptive, fault-tolerant and elastic
      framework, to run large-scale sensitivity analysis or deep-surrogate
      training on supercomputers.
      This package builds the API used when instrumenting the clients." )
   (license bsd-3)
   )
  )

;; Return the package object define above at the end of the module.
melissa-api

;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(define-module (adios2)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
;;  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-science)
;;  #:use-module (guix-hpc packages python-science)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages fabric-management)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages python)
  #:use-module (guix licenses))

(define commit
  "aac4a45fdd05fda62a80b1f5a4d174faade32f3c")

(define version
  "2.9.2")

(define revision
  "0")


(define adios2-source
  (origin
    (method git-fetch)
    (uri (git-reference (url "https://github.com/ornladios/ADIOS2.git")
                        (commit commit)))
(sha256 (base32 "01g27pbx28502bnnfmpihmsvms6ws0fzdmi2qia9vfb1h23icv2n"))))

(define adios2-version
  (git-version version revision commit))

(define adios2-license
  asl2.0)

(define adios2-homepage
  "https://github.com/ornladios/ADIOS2")

(define-public adios2
  (package
    (name "adios2")
    (version adios2-version)
    (source
     adios2-source)
    (build-system cmake-build-system)
    (inputs (list perl openmpi ucx libfabric zeromq hdf5 python python-mpi4py python-numpy libffi gcc-toolchain gfortran-toolchain pkg-config ))
    (arguments
     '(#:tests? #f))
    (home-page adios2-homepage)
    (synopsis "ADIOS2: The Adaptable Input Output System version 2")
    (description
     "ADIOS2 transports data as groups of self-describing variables and attributes across different media types (such as files, wide-area-networks, and remote direct memory access) using a common application programming interface for all transport modes. ADIOS2 can be used on supercomputers, cloud systems, and personal computers.")
    (license adios2-license)))


(define adios2-2.10
  (package
    (inherit adios2)
    (version "v2.10.0-rc1")
    (source (origin
	     (method git-fetch)
	     (uri (git-reference (url "https://github.com/ornladios/ADIOS2.git")
				 (commit "68f8b41b75204835b498c42f56f8d479f1685536")))
	     (sha256 (base32
                      "1z19ifn6s7v5lwz8hahpr77zfp2lbckl2aw1qy590089rz1bqgw4"))))
    ))
    
adios2
